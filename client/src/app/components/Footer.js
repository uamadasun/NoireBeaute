import React from "react";

const Footer = () => {
  return (
    <div>
      <footer
        className="
             text-xs text-white text-center
             fixed
             inset-x-0
             bottom-0
             p-4
             mt-10 footer"
      >
        Copyright &copy; 2023 Noire Beaute by J. Developed and Designed by <a href="https://uchenna.io" target="_blank">Amadasun Web Design, LLC</a>.
      </footer>
    </div>
  );
};

export default Footer;
