exports.id = 869;
exports.ids = [869];
exports.modules = {

/***/ 1449:
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

Promise.resolve(/* import() eager */).then(__webpack_require__.t.bind(__webpack_require__, 1232, 23));
Promise.resolve(/* import() eager */).then(__webpack_require__.t.bind(__webpack_require__, 6926, 23));
Promise.resolve(/* import() eager */).then(__webpack_require__.t.bind(__webpack_require__, 831, 23));
Promise.resolve(/* import() eager */).then(__webpack_require__.t.bind(__webpack_require__, 4282, 23));
Promise.resolve(/* import() eager */).then(__webpack_require__.t.bind(__webpack_require__, 2987, 23))

/***/ }),

/***/ 2462:
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

Promise.resolve(/* import() eager */).then(__webpack_require__.bind(__webpack_require__, 6223))

/***/ }),

/***/ 6223:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ components_NavBar)
});

// EXTERNAL MODULE: external "next/dist/compiled/react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(6786);
// EXTERNAL MODULE: external "next/dist/compiled/react"
var react_ = __webpack_require__(8038);
// EXTERNAL MODULE: ./node_modules/@headlessui/react/dist/components/dialog/dialog.js + 47 modules
var dialog = __webpack_require__(6530);
// EXTERNAL MODULE: ./node_modules/@heroicons/react/24/outline/esm/Bars3Icon.js
var Bars3Icon = __webpack_require__(6140);
// EXTERNAL MODULE: ./node_modules/@heroicons/react/24/outline/esm/XMarkIcon.js
var XMarkIcon = __webpack_require__(7048);
;// CONCATENATED MODULE: ./src/app/assets/logos/logo.png
/* harmony default export */ const logo = ({"src":"/_next/static/media/logo.d8347fb1.png","height":575,"width":1671,"blurDataURL":"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAADCAMAAACZFr56AAAALVBMVEX19erdvXTdvmvHmkLTsWrt047y6uDRsXTixnXo0YnkxInMo2Hx2JHz8/fjw4NoVi5IAAAAD3RSTlMGaqVpF3Ixl7CnJ3i7QkANl0x/AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAI0lEQVR4nGPgY2Jh5GBmYGBg42Ri5WHnZWDgYmFmZOVmYwAABrwAf2/mGjUAAAAASUVORK5CYII=","blurWidth":8,"blurHeight":3});
// EXTERNAL MODULE: ./node_modules/next/image.js
var next_image = __webpack_require__(2451);
var image_default = /*#__PURE__*/__webpack_require__.n(next_image);
;// CONCATENATED MODULE: ./src/app/components/NavBar.js
/* __next_internal_client_entry_do_not_use__ default auto */ 




// import Logo from "../assets/logo.png";



const NavBar = ({ children })=>{
    // const navigate = useNavigate();
    const navigation = [
        {
            name: "Home",
            href: "/"
        },
        {
            name: "Services",
            href: "/services"
        },
        {
            name: "Location",
            href: "/location"
        },
        {
            name: "Contact",
            href: "/contact"
        }
    ];
    const [mobileMenuOpen, setMobileMenuOpen] = (0,react_.useState)(false);
    return /*#__PURE__*/ jsx_runtime_.jsx("div", {
        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("header", {
            className: "absolute inset-x-0 top-0 z-50",
            children: [
                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("nav", {
                    className: "flex items-center justify-between p-6 lg:px-8",
                    "aria-label": "Global",
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: "flex lg:flex-1",
                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("a", {
                                href: "/",
                                className: "-m-1.5 p-1.5",
                                children: [
                                    /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                        className: "sr-only",
                                        children: "Noire Beaute by J"
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx((image_default()), {
                                        className: "h-10 w-10",
                                        src: logo,
                                        alt: "noire beaute by j logo",
                                        width: 500,
                                        height: 500
                                    })
                                ]
                            })
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: "flex lg:hidden",
                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("button", {
                                type: "button",
                                className: "-m-2.5 inline-flex items-center justify-center rounded-md p-2.5 text-white",
                                onClick: ()=>setMobileMenuOpen(true),
                                children: [
                                    /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                        className: "sr-only",
                                        children: "Open main menu"
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx(Bars3Icon/* default */.Z, {
                                        className: "h-6 w-6",
                                        "aria-hidden": "true"
                                    })
                                ]
                            })
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: "hidden lg:flex lg:gap-x-12",
                            children: navigation.map((item)=>/*#__PURE__*/ jsx_runtime_.jsx("a", {
                                    href: item.href,
                                    className: "text-sm font-semibold leading-6 text-white",
                                    children: item.name
                                }, item.name))
                        })
                    ]
                }),
                /*#__PURE__*/ (0,jsx_runtime_.jsxs)(dialog/* Dialog */.V, {
                    as: "div",
                    className: "lg:hidden",
                    open: mobileMenuOpen,
                    onClose: setMobileMenuOpen,
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: "fixed inset-0 z-50"
                        }),
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)(dialog/* Dialog */.V.Panel, {
                            className: "fixed inset-y-0 right-0 z-50 w-3/4 overflow-y-auto bg-black px-6 py-6 sm:max-w-sm sm:ring-1 sm:ring-white",
                            children: [
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                    className: "flex items-center justify-between",
                                    children: [
                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("a", {
                                            href: "/",
                                            className: "-m-1.5 p-1.5",
                                            children: [
                                                /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                    className: "sr-only",
                                                    children: "Noire Beaute by J"
                                                }),
                                                /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                    className: "h-20 w-20",
                                                    src: logo,
                                                    alt: ""
                                                })
                                            ]
                                        }),
                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("button", {
                                            type: "button",
                                            className: "-m-2.5 rounded-md p-2.5 text-white",
                                            onClick: ()=>setMobileMenuOpen(false),
                                            children: [
                                                /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                    className: "sr-only",
                                                    children: "Close menu"
                                                }),
                                                /*#__PURE__*/ jsx_runtime_.jsx(XMarkIcon/* default */.Z, {
                                                    className: "h-6 w-6",
                                                    "aria-hidden": "true"
                                                })
                                            ]
                                        })
                                    ]
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                    className: "mt-6 flow-root",
                                    children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                        className: "-my-6 divide-y divide-white-500/10",
                                        children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                            className: "space-y-2 py-6",
                                            children: navigation.map((item)=>/*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                    href: item.href,
                                                    className: "-mx-3 block rounded-lg px-3 py-2 text-base font-semibold leading-7 text-white hover:bg-gray-50 hover:text-black",
                                                    children: item.name
                                                }, item.name))
                                        })
                                    })
                                })
                            ]
                        })
                    ]
                })
            ]
        })
    });
};
/* harmony default export */ const components_NavBar = (NavBar);


/***/ }),

/***/ 9447:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ RootLayout),
  metadata: () => (/* binding */ metadata)
});

// EXTERNAL MODULE: external "next/dist/compiled/react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(6786);
// EXTERNAL MODULE: ./node_modules/next/font/google/target.css?{"path":"src/app/layout.js","import":"Inter","arguments":[{"subsets":["latin"]}],"variableName":"inter"}
var layout_js_import_Inter_arguments_subsets_latin_variableName_inter_ = __webpack_require__(7647);
var layout_js_import_Inter_arguments_subsets_latin_variableName_inter_default = /*#__PURE__*/__webpack_require__.n(layout_js_import_Inter_arguments_subsets_latin_variableName_inter_);
// EXTERNAL MODULE: ./node_modules/next/dist/compiled/react/react.shared-subset.js
var react_shared_subset = __webpack_require__(2947);
;// CONCATENATED MODULE: ./src/app/components/Footer.js


const Footer = ()=>{
    return /*#__PURE__*/ jsx_runtime_.jsx("div", {
        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("footer", {
            className: " text-xs text-white text-center fixed inset-x-0 bottom-0 p-4 mt-10 footer",
            children: [
                "Copyright \xa9 2023 Noire Beaute by J. Developed and Designed by ",
                /*#__PURE__*/ jsx_runtime_.jsx("a", {
                    href: "https://uchenna.io",
                    target: "_blank",
                    children: "Amadasun Web Design, LLC"
                }),
                "."
            ]
        })
    });
};
/* harmony default export */ const components_Footer = (Footer);

// EXTERNAL MODULE: ./node_modules/next/dist/build/webpack/loaders/next-flight-loader/module-proxy.js
var module_proxy = __webpack_require__(1363);
;// CONCATENATED MODULE: ./src/app/components/NavBar.js

const proxy = (0,module_proxy.createProxy)(String.raw`/Users/uchennaamadasun/Documents/Projects/NoireBeauteByJ/client/src/app/components/NavBar.js`)

// Accessing the __esModule property and exporting $$typeof are required here.
// The __esModule getter forces the proxy target to create the default export
// and the $$typeof value is for rendering logic to determine if the module
// is a client boundary.
const { __esModule, $$typeof } = proxy;
const __default__ = proxy.default;


/* harmony default export */ const NavBar = (__default__);
// EXTERNAL MODULE: ./src/app/globals.css
var globals = __webpack_require__(5023);
;// CONCATENATED MODULE: ./src/app/layout.js





const metadata = {
    title: "Create Next App",
    description: "Generated by create next app"
};
function RootLayout({ children }) {
    return /*#__PURE__*/ jsx_runtime_.jsx("html", {
        lang: "en",
        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("body", {
            className: (layout_js_import_Inter_arguments_subsets_latin_variableName_inter_default()).className,
            children: [
                /*#__PURE__*/ jsx_runtime_.jsx(NavBar, {}),
                children,
                /*#__PURE__*/ jsx_runtime_.jsx(components_Footer, {})
            ]
        })
    });
}


/***/ }),

/***/ 3881:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var next_dist_lib_metadata_get_metadata_route__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(85);
/* harmony import */ var next_dist_lib_metadata_get_metadata_route__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(next_dist_lib_metadata_get_metadata_route__WEBPACK_IMPORTED_MODULE_0__);
  

  /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ((props) => {
    const imageData = {"type":"image/x-icon","sizes":"16x16"}
    const imageUrl = (0,next_dist_lib_metadata_get_metadata_route__WEBPACK_IMPORTED_MODULE_0__.fillMetadataSegment)(".", props.params, "favicon.ico")

    return [{
      ...imageData,
      url: imageUrl + "",
    }]
  });

/***/ }),

/***/ 5023:
/***/ (() => {



/***/ })

};
;